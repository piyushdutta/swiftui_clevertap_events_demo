//
//  ContentView.swift
//  Clevertap_working
//
//  Created by piyush dutta on 09/10/19.
//  Copyright © 2019 piyush dutta. All rights reserved.
//

import SwiftUI
import CleverTapSDK

struct ContentView: View {
    var body : some View{
        Button(action: {
            let profile: Dictionary<String, String> = [
                //Update pre-defined profile properties
                "Name": "Jack Montana",
                "Email": "jack.montana@gmail.com",
                //Update custom profile properties
                "Plan type": "Silver",
                "Favorite Food": "Pizza"
            ]

            CleverTap.sharedInstance()?.profilePush(profile)
        }) {
            Text("enter event here")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
    
}
